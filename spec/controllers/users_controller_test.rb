require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  render_views

  describe "#GET #home" do
    before do
      get :home
    end

    it "200 OK が返ってくる" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "適切なテンプレートが表示されているか" do
      expect(response).to render_template :home
    end
  end
end
