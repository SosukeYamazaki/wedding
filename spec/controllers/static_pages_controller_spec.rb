require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  render_views

  describe "#GET #home" do
    before do
      get :home
    end

    it "200 OK が返ってくる" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "適切なテンプレートが表示されているか" do
      expect(response).to render_template :home
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
      assert_select "title", "HappyWedding"
    end
  end
end
